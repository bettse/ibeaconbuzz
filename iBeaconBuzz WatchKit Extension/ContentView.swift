//
//  ContentView.swift
//  iBeaconBuzz WatchKit Extension
//
//  Created by Eric Betts on 8/11/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
