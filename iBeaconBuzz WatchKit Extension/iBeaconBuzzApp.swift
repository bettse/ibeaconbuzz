//
//  iBeaconBuzzApp.swift
//  iBeaconBuzz WatchKit Extension
//
//  Created by Eric Betts on 8/11/21.
//

import SwiftUI

@main
struct iBeaconBuzzApp: App {
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
