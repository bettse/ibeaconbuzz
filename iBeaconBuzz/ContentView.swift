//
//  ContentView.swift
//  iBeaconBuzz
//
//  Created by Eric Betts on 8/11/21.
//

import SwiftUI
import CoreLocation

struct ContentView: View {
    @EnvironmentObject var beaconManagement: BeaconManagement
    var body: some View {
        VStack(alignment: .center) {
            Text("Hello, world!")
                .foregroundColor(colorForDistance(beaconManagement.distance))
                .padding()
        }
        .onAppear(perform: self.beaconManagement.start)
    }

    func colorForDistance(_ distance: CLProximity) -> Color {
        switch distance {
            case .far:
                return .blue
            case .near:
                return .orange
            case .immediate:
                return .red
            case .unknown:
                return .gray
            default:
                return .gray
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(BeaconManagement.shared)
    }
}
