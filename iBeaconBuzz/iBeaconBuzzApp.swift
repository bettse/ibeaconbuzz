//
//  iBeaconBuzzApp.swift
//  iBeaconBuzz
//
//  Created by Eric Betts on 8/11/21.
//

import SwiftUI

@main
struct iBeaconBuzzApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(BeaconManagement.shared)
        }
    }
}
