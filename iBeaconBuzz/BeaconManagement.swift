//
//  BeaconManagement.swift
//  iBeaconBuzz
//
//  Created by Eric Betts on 8/11/21.
//

import Foundation
import CoreLocation
import AVFoundation

class BeaconManagement : NSObject, ObservableObject, CLLocationManagerDelegate {
    static let shared = BeaconManagement()

    let wikipediaExample = UUID(uuidString: "fb0b57a2-8228-44cd-913a-94a122ba1206")!
    let beaconID = "org.ericbetts.myBeaconRegion"
    let locationManager = CLLocationManager()
    var monitoring :Bool = false

    @Published private(set) var distance : CLProximity = .unknown

    override init() {
        super.init()
    }

    func start() {
        print("Start!")
        if locationManager.authorizationStatus == .authorizedAlways {
            monitorBeacons()
        } else {
            locationManager.requestAlwaysAuthorization()
        }
    }

    func monitorBeacons() {
        if (self.monitoring) {
            return
        }
        if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
            print("Monitor!")
            locationManager.delegate = self
            // Create the region and begin monitoring it.
            let region = CLBeaconRegion(uuid: wikipediaExample, identifier: beaconID)
            self.locationManager.startMonitoring(for: region)
            monitoring = true
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("didFailWIthError \(error)")
    }

    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        print("didStartMonitoringFor \(region)")
    }

    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
        print("didVisit")
    }

    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLBeaconRegion {
            print("didEnterRegion")
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            
            // Start ranging only if the devices supports this service.
            if CLLocationManager.isRangingAvailable() {
                manager.startRangingBeacons(satisfying: CLBeaconIdentityConstraint(uuid: wikipediaExample))
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("didExitRegion")
        manager.stopRangingBeacons(satisfying: CLBeaconIdentityConstraint(uuid: wikipediaExample))
    }

    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if manager.authorizationStatus == .authorizedAlways {
            monitorBeacons()
        }
    }

    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        print("didRangeBeacons")
        if beacons.count > 0 {
            let nearestBeacon = beacons.first!
            let major = CLBeaconMajorValue(truncating: nearestBeacon.major)
            let minor = CLBeaconMinorValue(truncating: nearestBeacon.minor)

            distance = nearestBeacon.proximity
            switch nearestBeacon.proximity {
            case .immediate:
                print("immediate \(major) \(minor)")
                break
            case .near:
                print("near \(major) \(minor)")
                break
            case .far:
                print("far \(major) \(minor)")
                break
            case .unknown:
                print("unknown \(major) \(minor)")
                break
            default:
                print("\(major) \(minor) \(nearestBeacon.proximity)")
                break
            }
        }
    }
}
